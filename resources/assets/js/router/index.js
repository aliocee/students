import Vue from 'vue'
import VueRouter from 'vue-router'
import { setStatus } from '../api'

Vue.use(VueRouter)

const router = new VueRouter({
    routes: [
    	{ path: '/', component: require('../views/utils/dashboard.vue')},
        { path: '/profile', component: require('../views/utils/profile.vue')},
        { path: '/change-password', component: require('../views/utils/change-password.vue')},
        { path: '/students', component: require('../views/students/index.vue')},
        { path: '/students/create', component: require('../views/students/form.vue')},
        { path: '/students/:id/edit', component: require('../views/students/form.vue'), meta: {mode: 'edit'}},
        { path: '/students/:id', component: require('../views/students/show.vue')},
        { path: '/students/:id/clone', component: require('../views/students/form.vue'), meta: {mode: 'clone'}},
        { path: '/students/:id/status/:type', beforeEnter: function(to, from, next) {
            setStatus(`students/${to.params.id}/status/${to.params.type}`, function(data) {
                if(data.saved) {
                    next(`/students/${to.params.id}?saved=${to.params.type}`)
                }
            }, function(error) {
                next(`/students/${to.params.id}?error=${to.params.type}`)
            })
        }},

    ]
})

export default router
