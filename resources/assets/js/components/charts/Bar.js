import Vue from 'vue'
import { Bar } from 'vue-chartjs'
import { getCount } from '../../api'

export default Bar.extend({
  mounted () {
  	var vm = this

  	getCount(`students/bar`, function(data) {
  		var options = {
  			responsive: true,
  			maintainAspectRatio: false,
  			scales: {
        			xAxes: [{
        				barPercentage: 0.7
        			}]
        		}
        	}
        vm.renderChart(data.bar, options)
    }, function(error) {
        console.log(error);
    })
  }
})