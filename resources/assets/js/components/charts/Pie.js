import Vue from 'vue'
import { Pie } from 'vue-chartjs'
import { getCount } from '../../api'

export default Pie.extend({
  mounted () {
  	var vm = this

  	getCount(`students/pie`, function(data) {
        vm.renderChart(data.pie)
    }, function(error) {
        console.log(error);
    })
  }
})