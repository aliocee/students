import Vue from 'vue'
import Vuex from 'vuex'

import * as types from './types'
import { getByIndex, getByPath, store, deletes, counters } from '../api'

Vue.use(Vuex)

const state = {
    model: {},
    loading: false,
    processing: false,
    errors: {},
    form: {
        items: []
    },
    option: {},
    counters: {}
}

const actions = {

    fetchIndex({commit, state}, payload) {
        commit(types.LOADING_BEGIN)
        commit(types.CLEAN_UP)
        getByIndex(state.route, payload.resource, function(data) {
            commit(types.SET_MODEL, data)
            commit(types.LOADING_END)
        }, function(error) {

        })
    },
    fetchById({commit, state}, payload) {
        commit(types.LOADING_BEGIN)
        commit(types.CLEAN_UP)
        getByPath(state.route, payload.path, function(data) {
            commit(types.SET_MODEL, data)
            commit(types.LOADING_END)
        }, function(error) {

        })
    },
    fetchFormById({commit, state}, payload) {
        commit(types.LOADING_BEGIN)
        getByPath(state.route, payload.path, function(data) {
            commit(types.SET_FORM, data)
            commit(types.LOADING_END)
        }, function(error) {

        })
    },
    fetchCounter({commit, state}, payload) {
        commit(types.LOADING_BEGIN)
        commit(types.CLEAN_UP)
        getCount(payload, payload.path, function(data) {
            commit(types.SET_COUNTERS, data)
            commit(types.LOADING_END)
        }, function(error) {

        })
    },
    storeById({commit, state}, payload) {
        commit(types.PROCESS_BEGIN)
        commit(types.CLEAR_FORM_ERROR)
        store(payload, function(data) {
            commit(types.PROCESS_END)
            payload.router.push(payload.redirect)
        }, function(error) {
            if(error.status === 422) {
                commit(types.SET_FORM_ERROR, error)
            }
            commit(types.PROCESS_END)
        })
    },
    deleteById({commit, state}, payload) {
        commit(types.PROCESS_BEGIN)
        commit(types.CLEAR_FORM_ERROR)
        deletes(payload, function() {
            commit(types.PROCESS_END)
        }, function(error) {
            commit(types.PROCESS_END)
        })
    }
}

const getters = {
    model(state) {
        return state.model
    },
    option(state) {
        return state.option
    },
    form(state) {
        return state.form
    },
    errors(state) {
        return state.errors
    },
    counters(state) {
        return state.counters
    }
}

const mutations = {
    [types.CLEAN_UP] (state, payload) {
        state.model = {}
        state.errors = {}
        state.form = {
            items: []
        }
        state.option = {}
        state.counters = {}
    },
    [types.SET_FORM_ERROR] (state, payload) {
        state.errors = payload.data
    },
    [types.CLEAR_FORM_ERROR] (state, payload) {
        state.errors = {}
    },
    [types.SET_FORM] (state, payload) {
        state.option = payload.option
        state.form = payload.form
    },
    [types.SET_MODEL] (state, payload) {
        state.model = payload.model
    },
    [types.SET_ITEMS] (state, payload) {
        state.form.items = payload.items
    },
    [types.LOADING_BEGIN] (state) {
        state.loading = true
    },
    [types.LOADING_END] (state) {
        state.loading = false
    },
    [types.PROCESS_BEGIN] (state) {
        state.processing = true
    },
    [types.PROCESS_END] (state) {
        state.processing = false
    },
    [types.SET_COUNTERS] (state, payload) {
        state.counters = payload.counters
    }
}

export default new Vuex.Store({
    state,
    actions,
    getters,
    mutations
})