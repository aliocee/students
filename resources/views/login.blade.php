<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Login - Student Information System</title>
        <link rel="shortcut icon" href="{{asset('images/favicon.png')}}">
        <link href="{{asset('css/app.css')}}" rel="stylesheet">
        <link href="{{asset('css/fontawesome.css')}}" rel="stylesheet">
        <link href="{{asset('css/login.css')}}" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
        <style type="text/css">
            @media only screen and (max-width: 768px) {
                /* For mobile phones: */
                .login .panel {
                    width: 90%;
                }
            }
        </style>
    </head>
    <body>
        
    <div class="center-vertical bg-black">
        <div class="center-content">
            <form id="login-validation" class="col-md-3 col-sm-3 col-xs-11 center-margin" action="{{url('login')}}" method="post">
                {{csrf_field()}}
                <h3 class="text-center pad25B font-gray font-size-23"><img src="{{asset('images/logo-admin.png')}}" alt="Webideas"></h3>
                <div id="login-form" class="content-box">
                    <div class="content-box-wrapper pad20A">
                        <div class="form-group">
                            <label for="Email">Email address:</label>
                            <div class="input-group input-group-lg"><span class="input-group-addon addon-inside bg-white font-primary"><i class="glyph-icon icon-envelope-o"></i></span>
                                <input type="email" name="email" class="form-control" id="Email" placeholder="Enter email" value="{{old('email')}}" autofocus>
                                @if($errors->has('email'))
                                    <small class="e-text">{{$errors->first('email')}}</small>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="Password">Password:</label>
                            <div class="input-group input-group-lg"><span class="input-group-addon addon-inside bg-white font-primary"><i class="glyph-icon icon-unlock-alt"></i></span>
                                <input type="password" name="password" class="form-control" id="Password" placeholder="Password">
                                @if($errors->has('password'))
                                    <small class="e-text">{{$errors->first('password')}}</small>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="checkbox-primary col-md-6" style="height: 20px">
                                <label>
                                    <input type="checkbox" id="loginCheckbox" class="custom-checkbox"> Remember me</label>
                            </div>

                        </div>
                    </div>
                    <div class="button-pane">
                        <button type="submit" class="btn btn-block btn-primary">Login</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    </body>
</html>
