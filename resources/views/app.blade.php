<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Student Information System</title>
        <link rel="shortcut icon" href="{{asset('images/favicon.png')}}">
        <!-- <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}"> -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />

        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/sweetalert2/6.4.2/sweetalert2.css">
        <link rel="stylesheet" type="text/css" href="{{asset('css/custom.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/print.css')}}">
    </head>
    <body>
        <div id="loading" style="display: none;"><div class="svg-icon-loader"><img src="{{asset('images/bars.svg')}}" width="40" alt=""></div></div>
        <div id="root"></div>
    </body>
    <script type="text/javascript" src="{{asset('js/app.js')}}"></script>

    <script type="text/javascript" src="{{asset('js/custom.js')}}"></script>
     <script type="text/javascript" src="https://cdn.jsdelivr.net/sweetalert2/6.4.2/sweetalert2.js"></script>
</html>
