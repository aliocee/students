<?php
Route::get('/', 'AppController@index');

Route::post('login', 'AppController@login')
    ->middleware('guest');
Route::get('logout', 'AppController@logout')
    ->middleware('auth');

Route::get('images/{filename}', 'AppController@image')
    ->middleware('auth');

Route::group(['prefix' => 'api/', 'middleware' => ['auth', 'api']], function() {
    // Students
    Route::get('students/counters', 'Admin\StudentController@counters');
    Route::get('students/pie', 'Admin\StudentController@pie');
    Route::get('students/bar', 'Admin\StudentController@bar');
    Route::get('students/barlga', 'Admin\StudentController@barlga');
    Route::post('students/{id}', 'Admin\StudentController@update');
    Route::get('students/{student}/pdf', 'Admin\StudentController@pdf');
    Route::put('students/{student}/status/{type}', 'Admin\StudentController@markAs');
    Route::resource('students', 'Admin\StudentController');

    // Notes
    Route::get('notes/search', 'NoteController@search');
    Route::resource('notes', 'NoteController');

    // User
    Route::get('user/sessionstatus', 'Admin\UserController@sessionstatus');
    Route::post('user/update', 'Admin\UserController@update');
    Route::post('user/changepassword', 'Admin\UserController@changepassword');

});