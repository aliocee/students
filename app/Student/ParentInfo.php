<?php

namespace App\Student;

use Illuminate\Database\Eloquent\Model;

class ParentInfo extends Model
{
    protected $fillable = [
        'student_id', 'fathers_name', 'mothers_name', 'phone', 'email', 'state_origin', 'next_of_kin',
    ];

    public static function initialize()
    {
        return [
            'fathers_name' => '',
            'mothers_name' => '',
            'phone' => '',
            'email' => '',
            'state_origin' => '',
            'next_of_kin' => '',
        ];
    }
}
