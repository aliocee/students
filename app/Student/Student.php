<?php

namespace App\Student;

use Illuminate\Database\Eloquent\Model;
use App\Support\PaginationOrderFilter;
use Illuminate\Support\Facades\Auth;
use App\Student\{Student, Note};
use App\Zone;
use App\User;

class Student extends Model
{
    use PaginationOrderFilter;

    protected $fillable = [
        'firstname', 'middlename', 'lastname', 'zone_id', 'gender', 'dob',
        'regno', 'status', 'image', 'state', 'lga', 'town', 'permanent_address', 'current_address', 'fathers_name', 'mothers_name', 'phone', 'email', 'state_origin', 'next_of_kin',
    ];

    protected $filterFields = [
        'id', 'firstname', 'middlename', 'lastname', 'zone_id', 'gender', 'dob', 'regno', 'status', 'created_at',

        // filter relation address
        'state', 'lga', 'town', 'permanent_address', 'current_address',

        // filter relation address
        'fathers_name', 'mothers_name', 'phone', 'email', 'state_origin', 'next_of_kin',

    ];

    public static function initialize()
    {
        $id = Auth::id();
        $user = User::find($id);

        return [
            'zone_id' => 1,
            'firstname' => '',
            'middlename' => '',
            'lastname' => '',
            'gender' => 'M',
            'dob' => '',
            'regno' => 'SKT/SS/' . mt_rand(10000, 99999),
            'status' => '0',
            'image' => 'default.jpg',
            'state' => '',
            'lga' => '',
            'town' => '',
            'permanent_address' => '',
            'current_address' => '',
            'fathers_name' => '',
            'mothers_name' => '',
            'phone' => '',
            'email' => '',
            'state_origin' => '',
            'next_of_kin' => '',
            'notes' => [
                Note::initialize()
            ],
        ];
    }

    public function notes()
    {
        return $this->hasMany(Note::class);
    }

    public function zone()
    {
        return $this->belongsTo(Zone::class);
    }
}
