<?php

namespace App\Student;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable = [
        'student_id', 'state', 'lga', 'town', 'current_address', 'permanent_address'
    ];

    public static function initialize()
    {
        return [
            'state' => '',
            'lga' => '',
            'town' => '',
            'current_address' => '',
            'permanent_address' => '',
        ];
    }
}
