<?php

namespace App\Student;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    protected $fillable = [
        'student_id', 'note',
    ];

    public static function initialize()
    {
        return [
            'note' => '',
        ];
    }
}
