<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use Hash;

use App\User;

use Image;

class UserController extends Controller
{
    //
    public function sessionstatus(){

    	return response()
            ->json(['user' => Auth::user() ? Auth::user() : null]);
    }

    public function update(Request $request){

    	$id = Auth::id();

    	$this->validate($request, [
            'firstname' => 'required|max:255',
            'lastname' => 'required|max:255',
            'job_title' => 'required|max:255',
            'email' => 'required|email',
        ]);

        if ($request->hasFile('image')) {
            /* File Upload Start*/
            $file = $request->file('image')->getClientOriginalName();
            if(isset($file)){
                $image = $request->file('image');
                $extension = $request->file('image')->getClientOriginalExtension();// File upload Extenstion
                $fileName = str_slug($request->firstname) . "_" . time() . "." . $extension;
                Image::make($image)
                    ->resize(150, 150)
                    ->save( public_path('/upload/images/' . $fileName ) );
            }
            else
            {
                $fileName = "";
            }
        /* File Upload End*/
        }

        $user = User::findOrFail($id);

        //dd($user);

        $data = $request->all();

        if(!empty($fileName)){
            $data['image'] = $fileName;
        }
        else{
            $data['image'] = $user->image;
        }

        $user->update($data);

    	return response()
            ->json([
                'saved' => true
            ]);
    }

    public function changepassword(Request $request){

        $id = Auth::id();

        $this->validate($request, [
            'password'     =>  'min:6|required',
            'newpassword'  =>  'min:6|required|different:password',
            'cnewpassword' =>  'min:6|required|same:newpassword'
        ]);

        $oldpassword = request('password');
        if(Hash::check($oldpassword, Auth::user()->password)){
            
            $user = User::findOrFail($id);

            $user->password = Hash::make(request('newpassword'));

            $user->save();
        }


        return response()
            ->json([
                'saved' => true
            ]);
    }


}
