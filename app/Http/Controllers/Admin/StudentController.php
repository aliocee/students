<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Student\ {
    Student,
    Address,
    ParentInfo,
    Note
};

use App\User;

use PDF;
use DB;
use Image;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $model = Student::with(['notes'])
            ->paginationOrderFilter();

        return response()
            ->json([
                'model' => $model
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()
            ->json([
                'form' => Student::initialize()
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'firstname' => 'required|max:255',
            // 'middlename' => 'required|max:255',
            'lastname' => 'required|max:255',
            'dob' => 'required|date_format:Y-m-d',
            'gender' => 'required',
            'image' => 'required|mimes:jpeg,jpg,png|max:2000',
            'state' => 'required|max:255',
            'lga' => 'required|max:255',
            'town' => 'required|max:255',
            'permanent_address' => 'required|max:255',
            'current_address' => 'required|max:255',
            'fathers_name' => 'required|max:255',
            'mothers_name' => 'required|max:255',
            'phone' => 'required|max:255',
            'email' => 'required|email',
            'state_origin' => 'required|max:255',
            'next_of_kin' => 'required|max:255',
            'notes' => 'required|array|min:1',
            'notes.*.note' => 'required|max:5000'
        ]);

        if ($request->hasFile('image')) {
            /* File Upload Start*/
            $file = $request->file('image')->getClientOriginalName();
            if(isset($file)){
                $image = $request->file('image');
                $extension = $request->file('image')->getClientOriginalExtension();// File upload Extenstion
                $fileName = str_slug($request->firstname) . "_" . time() . "." . $extension;
                Image::make($image)
                    ->resize(150, 150)
                    ->save( public_path('/upload/images/' . $fileName ) );
            }
            else
            {
                $fileName = "";
            }
        /* File Upload End*/
        }


        $notes = [];

        foreach($request->notes as $note) {
            $notes[] = new Note($note);
        }

        $data = $request->except('notes');

        $data['image'] = $fileName;

        $student = DB::transaction(function() use ($request, $data, $notes)
        {
            $student = new Student($data);

            $student->save();

            $student->notes()->saveMany($notes);

            return $student;
        });

        return response()
            ->json([
                'saved' => true
            ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $student = Student::with([
            'notes'
            ])
            ->findOrFail($id);

        return response()
            ->json([
                'model' => $student
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $student = Student::with([
            'notes'
            ])
            ->findOrFail($id);

        if(request()->get('convert') == 'clone') {
            $student->regno = 'SKT/SS/' . mt_rand(10000, 99999);
        }

        return response()
            ->json([
                'form' => $student
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'firstname' => 'required|max:255',
            // 'middlename' => 'required|max:255',
            'lastname' => 'required|max:255',
            'dob' => 'required|date_format:Y-m-d',
            'gender' => 'required',
            'state' => 'required|max:255',
            'lga' => 'required|max:255',
            'town' => 'required|max:255',
            'permanent_address' => 'required|max:255',
            'current_address' => 'required|max:255',
            'fathers_name' => 'required|max:255',
            'mothers_name' => 'required|max:255',
            'phone' => 'required|max:255',
            'email' => 'required|email',
            'state_origin' => 'required|max:255',
            'next_of_kin' => 'required|max:255',
            'notes' => 'required|array|min:1',
            'notes.*.id' => 'integer|exists:notes,id,student_id,'.$id,
            'notes.*.note' => 'required|max:5000'
        ]);

        if ($request->hasFile('image')) {
            /* File Upload Start*/
            $file = $request->file('image')->getClientOriginalName();
            if(isset($file)){
                $image = $request->file('image');
                $extension = $request->file('image')->getClientOriginalExtension();// File upload Extenstion
                $fileName = str_slug($request->firstname) . "_" . time() . "." . $extension;
                Image::make($image)
                    ->resize(150, 150)
                    ->save( public_path('/upload/images/' . $fileName ) );
            }
            else
            {
                $fileName = "";
            }
        /* File Upload End*/
        }

        $student = Student::findOrFail($id);

        $notes = [];

        $noteIds = [];

        foreach($request->notes as $note) {
            if(isset($note['id'])) {
                Note::where('student_id', $student->id)
                    ->where('id', $note['id'])
                    ->update($note);

                $noteIds[] = $note['id'];
            } else {
                $notes[] = new Note($note);
            }
        }

        $data = $request->except('notes');

        if(!empty($fileName)){
            $data['image'] = $fileName;
        }
        else{
            $data['image'] = $student->image;
        }

        $student->update($data);

        Note::whereNotIn('id', $noteIds)
            ->where('student_id', $student->id)
            ->delete();

        if(count($notes)) {
            $student->notes()->saveMany($notes);
        }

        return response()
            ->json([
                'saved' => true
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $student = Student::findOrFail($id);

        $student->delete();

        return response()
            ->json([
                'deleted' => true
            ]);
    }

    public function markAs($id, $type)
    {
        switch ($type) {
            case 'register':
                $model = Student::findOrFail($id);
                $model->status = '1';
                $model->save();
                break;

            case 'left':
                $model = Student::findOrFail($id);
                $model->status = '0';
                $model->save();
                break;

            default:
                abort(404, 'Unknown Status');
                break;
        }

        return response()
            ->json([
                'saved' => true
            ]);
    }

    public function counters()
    {
        return response()
            ->json([
                'counters' => [
                    'total' => Student::count(),
                    'registered' => Student::where('status', '1')->count(),
                    'left' => Student::where('status', '0')->count(),
                    'users' => User::count()
                ]
            ]);
    }
    public function pie()
    {
        $m = Student::where('gender', 'M')->count();
        $f = Student::where('gender', 'F')->count();
        $pie = [
            'labels' => [
                "Female",
                "Male"
            ],
            'datasets' => array([
                'backgroundColor' => [
                    '#fd8c86','#ecedf1'
                ],
                'data' => [$f, $m]
            ])
        ];

        return response()
            ->json([
                'pie' => $pie
            ]);

    }

    public function bar()
    {
        $bsm = DB::table('students')
                 ->select('zone_id', DB::raw('count(zone_id) as count'))
                 ->where('gender', 'M')
                 ->groupBy('zone_id')
                 ->get()->toArray();
        $bsf = DB::table('students')
                 ->select('zone_id', DB::raw('count(zone_id) as count'))
                 ->where('gender', 'F')
                 ->groupBy('zone_id')
                 ->get()->toArray();

        $cntm = [];
        foreach($bsm as $b){
            $cntm[] = $b->count;
        }
        $cntf = [];
        foreach($bsf as $b){
            $cntf[] = $b->count;
        }
        $color1 = array_fill(0, count($bsf), '#F7D6C7');
        $color2 = array_fill(0, count($bsf), '#fd8c86');
        $bar = [
            'labels' => ['Zone 1', 'Zone 2', 'Zone 3', 'Zone 4', 'Zone 5', 'Zone 6'],
            'datasets' => array([
                'label' => 'Male',
                'backgroundColor' => $color2,
                'data' => $cntm
            ],
            [
                'label' => 'Female',
                'backgroundColor' => $color1,
                'data' => $cntf
            ])
        ];


        return response()
            ->json([
                'bar' => $bar
            ]);

    }

    public function barlga()
    {
        $lg = DB::table('students')
                 ->select('lga', DB::raw('count(*) as count'))
                 ->groupBy('lga')
                 ->get()->toArray();

        $cbt = [];
        $lgas = [];
        foreach($lg as $l){
            $cbt[] = $l->count;
            $lgas[] = $l->lga;
        }

        $color = array_fill(0, count($lgas), '#fd8c86');
        $lga = [
            'labels' => $lgas,
            'datasets' => array([
                'label' => 'Local Government',
                'backgroundColor' => $color,
                'data' => $cbt
            ])
        ];

        return response()
            ->json([
                'barlga' => $lga
            ]);

    }
}
