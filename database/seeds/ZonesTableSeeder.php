<?php

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\Zone;

class ZonesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        $zones = ['Zone 1','Zone 2','Zone 3','Zone 4','Zone 5','Zone 6'];

        foreach($zones as $zone) {
            Zone::create([
                'name' => $zone,
            ]);
        }
    }
}
