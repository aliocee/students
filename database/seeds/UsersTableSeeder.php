<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::insert([
            'firstname' => 'Aliyu',
            'lastname' => 'Musa',
            'email' => 'example@gmail.com',
            'job_title' => 'Admin',
            'username' => 'aliocee',
            'image' => 'default.jpg',
            'password' => bcrypt('123456'),
        ]);
    }
}
