<?php

use Illuminate\Database\Seeder;

use App\Student\{Student, Note};
use Faker\Factory;

class StudentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	$faker = Factory::create();

        foreach(range(1, 5784) as $i) {

            $student = Student::create([
                'zone_id' => $faker->numberBetween(1, 6),
                'firstname' => $faker->firstName($gender = 'male'|'female'),
                'middlename' => $faker->firstNameMale,
                'lastname' => $faker->firstNameMale,
                'gender' => $faker->randomElement($array = array ('M','F')),
                'dob' => $faker->date($format = 'Y-m-d', $max = 'now'),
                'regno' => 'SKT/SS/'.$faker->numberBetween(10000, 90000),
                'status' => $faker->randomElement($array = array ('0','1')),
                'image' => 'default.jpg',
                'state' => 'Sokoto',
                'lga' => $faker->randomElement($array = array('Binji','Bodinga','Dange Shuni','Gada','Goronyo','Gudu','Gwadabawa','Illela','Isa','Kebbe','Kware','Rabah','Sabon Birni','Shagari','Silame','Sokoto North','Sokoto South','Tambuwal','Tangaza','Tureta','Wamako','Wurno','Yabo')),
                'town' => $faker->city,
                'current_address' => $faker->streetAddress,
                'permanent_address' => $faker->streetAddress,
                'fathers_name' => $faker->name($gender = 'male'),
                'mothers_name' => $faker->name($gender = 'female'),
                'phone' => $faker->phoneNumber,
                'email' => $faker->safeEmail,
                'state_origin' => 'Sokoto',
                'next_of_kin' => $faker->name($gender = 'male'|'female'),
            ]);

        	Note::create([
                'student_id' => $student->id,
                'note' => $faker->text,
            ]);

        }
    }
}
