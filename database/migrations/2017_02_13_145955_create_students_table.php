<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('zone_id')->unsigned()->index();
            $table->string('regno');
            $table->string('firstname');
            $table->string('middlename');
            $table->string('lastname');
            $table->string('dob');
            $table->enum('gender', ['M', 'F']);
            $table->enum('status', ['0', '1']);
            $table->string('state');
            $table->string('lga');
            $table->string('town');
            $table->string('current_address');
            $table->string('permanent_address');
            $table->string('fathers_name');
            $table->string('mothers_name');
            $table->string('phone');
            $table->string('email');
            $table->string('state_origin');
            $table->string('next_of_kin');
            $table->string('image', 250);
            $table->timestamps();

            $table->foreign('zone_id')
                  ->references('id')->on('zones')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
